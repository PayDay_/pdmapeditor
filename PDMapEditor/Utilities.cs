﻿using OpenTK;
using System;

namespace PDMapEditor
{
    public static class Utilities
    {
        public static int OBBCount;
        public static int SphereCount;
        public static int TriangleCount;

        public static T Clamp<T>(T value, T min, T max) where T : IComparable<T>
        {
            if (value.CompareTo(min) < 0)
                return min;
            if (value.CompareTo(max) > 0)
                return max;

            return value;
        }

        public static int CountCharacters(string input, char inputCharacter)
        {
            int count = 0;
            char[] chars = input.ToCharArray();
            foreach (char character in chars)
            {
                if (character == inputCharacter)
                    count++;
            }
            return count;
        }

        public static float IntersectSphere(Ray ray, Mesh mesh, Vector3 center, float radius)
        {
            SphereCount++;

            Vector3 m = ray.Start - center;
            float b = Vector3.Dot(m, ray.Direction);
            float c = Vector3.Dot(m, m) - radius * radius;

            // Exit if r’s origin outside s (c > 0) and r pointing away from s (b > 0) 
            if (c > 0.0f && b > 0.0f) return -1;
            float discr = b * b - c;

            // A negative discriminant corresponds to ray missing sphere 
            if (discr < 0.0f) return -1;

            // Ray now found to intersect sphere, compute smallest t value of intersection
            float t = -b - (float)Math.Sqrt(discr);

            // If t is negative, ray started inside sphere so clamp t to zero 
            if (t < 0.0f)
                t = 0.0f;

            return t;
        }

        public static float IntersectMesh(Ray ray, Mesh mesh)
        {
            if (IntersectOBB(ray, mesh, mesh.BoundingMin * mesh.Scale, mesh.BoundingMax * mesh.Scale, mesh.ModelMatrix) == -1)
            {
                return -1;
            }

            for (int i = 0; i < mesh.IndexCount; i += 3)
            {
                float t = IntersectTriangle(ray, Vector3.TransformPerspective(mesh.Vertices[mesh.Indices[i]], mesh.ModelMatrix), Vector3.TransformPerspective(mesh.Vertices[mesh.Indices[i + 1]], mesh.ModelMatrix), Vector3.TransformPerspective(mesh.Vertices[mesh.Indices[i + 2]], mesh.ModelMatrix));
                if (t != -1)
                {
                    return t;
                }
            }

            return -1;
        }

        public static float IntersectTriangle(Ray ray, Vector3 v0, Vector3 v1, Vector3 v2)
        {
            TriangleCount++;

            Vector3 v0v1 = v1 - v0;
            Vector3 v0v2 = v2 - v0;
            Vector3 pvec = Vector3.Cross(ray.Direction, v0v2);
            float det = Vector3.Dot(v0v1, pvec);

            if (det < float.Epsilon) return -1;

            float invDet = 1 / det;

            Vector3 tvec = ray.Start - v0;
            float u = Vector3.Dot(tvec, pvec) * invDet;
            if (u < 0 || u > 1) return -1;

            Vector3 qvec = Vector3.Cross(tvec, v0v1);
            float v = Vector3.Dot(ray.Direction, qvec) * invDet;
            if (v < 0 || u + v > 1) return -1;

            float t = Vector3.Dot(v0v2, qvec) * invDet;

            return t;
        }

        /*public static float IntersectAABB(Ray ray, Vector3 min, Vector3 max)
        { 
            float tmin, tmax, tymin, tymax, tzmin, tzmax;
            int[] sign = new int[3];
            Vector3[] bounds = { min, max };

            sign[0] = ray.DirectionInverse.X < 0 ? 1 : 0;
            sign[1] = ray.DirectionInverse.Y < 0 ? 1 : 0;
            sign[2] = ray.DirectionInverse.Z < 0 ? 1 : 0;

            tmin = (bounds[sign[0]].X - ray.Start.X) * ray.DirectionInverse.X; 
            tmax = (bounds[1 - sign[0]].X - ray.Start.X) * ray.DirectionInverse.X; 
            tymin = (bounds[sign[1]].Y - ray.Start.Y) * ray.DirectionInverse.Y; 
            tymax = (bounds[1 - sign[1]].Y - ray.Start.Y) * ray.DirectionInverse.Y; 
 
            if ((tmin > tymax) || (tymin > tmax)) 
                return -1; 
            if (tymin > tmin) 
                tmin = tymin; 
            if (tymax<tmax) 
                tmax = tymax; 
 
            tzmin = (bounds[sign[2]].Z - ray.Start.Z) * ray.DirectionInverse.Z; 
            tzmax = (bounds[1 - sign[2]].Z - ray.Start.Z) * ray.DirectionInverse.Z; 
 
            if ((tmin > tzmax) || (tzmin > tmax)) 
                return -1; 
            if (tzmin > tmin) 
                tmin = tzmin; 
            if (tzmax<tmax) 
                tmax = tzmax; 
 
            return tmin; 
        }*/

        public static float IntersectOBB(Ray ray, Mesh mesh, Vector3 min, Vector3 max, Matrix4 modelMatrix)
        {
            OBBCount++;
            // Intersection method from Real-Time Rendering and Essential Mathematics for Games

            float tMin = 0.0f;
            float tMax = 100000f;

            Vector3 OBBposition_worldspace = modelMatrix.Row3.Xyz;

            Vector3 delta = OBBposition_worldspace - ray.Start;

            // Test intersection with the 2 planes perpendicular to the OBB's X axis
            {
                Vector3 xaxis = modelMatrix.Row0.Xyz.Normalized();
                float e = Vector3.Dot(xaxis, delta);
                float f = Vector3.Dot(ray.Direction, xaxis);

                if (Math.Abs(f) > 0.001f)
                { // Standard case

                    float t1 = (e + min.X) / f; // Intersection with the "left" plane
                    float t2 = (e + max.X) / f; // Intersection with the "right" plane
                                                     // t1 and t2 now contain distances betwen ray origin and ray-plane intersections

                    // We want t1 to represent the nearest intersection, 
                    // so if it's not the case, invert t1 and t2
                    if (t1 > t2)
                    {
                        float w = t1; t1 = t2; t2 = w; // swap t1 and t2
                    }

                    // tMax is the nearest "far" intersection (amongst the X,Y and Z planes pairs)
                    if (t2 < tMax)
                        tMax = t2;
                    // tMin is the farthest "near" intersection (amongst the X,Y and Z planes pairs)
                    if (t1 > tMin)
                        tMin = t1;

                    // And here's the trick :
                    // If "far" is closer than "near", then there is NO intersection.
                    // See the images in the tutorials for the visual explanation.
                    if (tMin > tMax)
                        return -1;

                }
                else
                { // Rare case : the ray is almost parallel to the planes, so they don't have any "intersection"
                    if (-e + min.X > 0.0f || -e + max.X < 0.0f)
                        return -1;
                }
            }


            // Test intersection with the 2 planes perpendicular to the OBB's Y axis
            // Exactly the same thing as above.
            {
                Vector3 yaxis = modelMatrix.Row1.Xyz.Normalized();
                float e = Vector3.Dot(yaxis, delta);
                float f = Vector3.Dot(ray.Direction, yaxis);

                if (Math.Abs(f) > 0.001f)
                {

                    float t1 = (e + min.Y) / f;
                    float t2 = (e + max.Y) / f;

                    if (t1 > t2) { float w = t1; t1 = t2; t2 = w; }

                    if (t2 < tMax)
                        tMax = t2;
                    if (t1 > tMin)
                        tMin = t1;

                    if (tMin > tMax)
                        return -1;
                }
                else
                {
                    if (-e + min.Y > 0.0f || -e + max.Y < 0.0f)
                        return -1;
                }
            }


            // Test intersection with the 2 planes perpendicular to the OBB's Z axis
            // Exactly the same thing as above.
            {
                Vector3 zaxis = modelMatrix.Row2.Xyz.Normalized();
                float e = Vector3.Dot(zaxis, delta);
                float f = Vector3.Dot(ray.Direction, zaxis);

                if (Math.Abs(f) > 0.001f)
                {

                    float t1 = (e + min.Z) / f;
                    float t2 = (e + max.Z) / f;

                    if (t1 > t2) { float w = t1; t1 = t2; t2 = w; }

                    if (t2 < tMax)
                        tMax = t2;
                    if (t1 > tMin)
                        tMin = t1;

                    if (tMin > tMax)
                        return -1;
                }
                else
                {
                    if (-e + min.Z > 0.0f || -e + max.Z < 0.0f)
                        return -1;
                }
            }

            return tMin;
        }

        private static void Swap(float a, float b)
        {
            float save = a;
            a = b;
            b = save;
        }

        public static void LogCounts()
        {
            Console.WriteLine("OBBCount: " + OBBCount);
            Console.WriteLine("SphereCount: " + SphereCount);
            Console.WriteLine("TriangleCount: " + TriangleCount);

            OBBCount = 0;
            SphereCount = 0;
            TriangleCount = 0;
        }
    }
}
