﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDMapEditor
{
    public class Ray
    {
        public readonly Vector3 Start;
        public readonly Vector3 End;
        public readonly Vector3 Direction;
        public readonly Vector3 DirectionInverse;
        public readonly float Length;

        public Ray(Vector2 screen, float length = 100000)
        {
            float mouseX = 2.0f * screen.X / Program.GLControl.Width - 1;
            float mouseY = (2.0f * screen.Y / Program.GLControl.Height - 1);

            Vector3 view = Program.Camera.LookAt - Program.Camera.Position;
            view.Normalize();

            Vector3 h = Vector3.Cross(view, Program.Camera.Up);
            h.Normalize();

            Vector3 v = Vector3.Cross(h, view);
            v.Normalize();

            float vLength = (float)Math.Tan(Program.Camera.FieldOfView / 2) * Program.Camera.NearClipDistance;
            float hLength = vLength * ((float)Program.GLControl.Width / Program.GLControl.Height);

            v *= vLength;
            h *= hLength;

            // linear combination to compute intersection of picking ray with
            // view port plane
            Vector3 pos = Program.Camera.Position + view * Program.Camera.NearClipDistance + h * mouseX + v * mouseY;

            // compute direction of picking ray by subtracting intersection point
            // with camera position
            Vector3 dir = pos - Program.Camera.Position;
            dir.Normalize();
            Vector3 rayEnd = pos + dir * length;

            Start = pos;
            End = rayEnd;
            Direction = dir;
            DirectionInverse = new Vector3((float)1 / Direction.X, (float)1 / Direction.Y, (float)1 / Direction.Z);
            Length = length;
        }
    }
}
